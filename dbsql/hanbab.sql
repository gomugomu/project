﻿--회원
CREATE TABLE USERS(
  USER_NO NUMBER(11) PRIMARY KEY,
  USER_ROLE VARCHAR2(20),
  USER_ID VARCHAR2(30) UNIQUE,
  USER_PWD VARCHAR2(90),
  NINKNAME VARCHAR2(30) UNIQUE,
  USER_NAME VARCHAR2(30),
  GENDER CHAR(1),
  PHONE VARCHAR2(13),
  E_MAIL VARCHAR2(50)
);

--파일관리
CREATE TABLE FILE_MANAGEMENT( 
  FILE_NO NUMBER(11) PRIMARY KEY,
  FILE_SIZE NUMBER(11),
  FILE_ORI_NAME VARCHAR2(100),
  FILE_SAVE_ROOT VARCHAR2(200),
  FILE_RENAME VARCHAR2(100)
);


-- 레시피
CREATE TABLE RECIEPE(
  REC_NO NUMBER(11) PRIMARY KEY,
  REC_DEP NUMBER(11),
  REC_TITLE VARCHAR2(100),
  REC_CONTENTS VARCHAR2(2000),
  REC_FOOD VARCHAR2(1000),
  REC_INGREDIENT VARCHAR2(2000),
  USER_NO NUMBER(11) REFERENCES USERS(USER_NO),
  REC_SCORE NUMBER(11),
  REC_COUNT NUMBER(11),
  FILE_NO NUMBER(11) REFERENCES FILE_MANAGEMENT(FILE_NO)  
);


--공모전
CREATE TABLE CONTEST(
  CON_NO NUMBER(11) PRIMARY KEY,
  SPONSOR_ID VARCHAR2(30) REFERENCES USERS(USER_ID),
  CON_ENTRY VARCHAR2(2000),
  REC_ENTRY VARCHAR2(2000),
  FILE_NO NUMBER(11) REFERENCES FILE_MANAGEMENT(FILE_NO),
  CON_TITLE VARCHAR2(100),
  CON_CONTENTS VARCHAR2(2000),
  CON_COUNT VARCHAR2(2000)  
);


--평가
CREATE TABLE EVALUATION(
  EV_NO NUMBER(11) PRIMARY KEY,
  CON_NO NUMBER(11) REFERENCES CONTEST(CON_NO),
  REC_NO NUMBER(11) REFERENCES RECIEPE(REC_NO),
  USER_NO NUMBER(11) REFERENCES USERS(USER_NO),
  EV_SCORE NUMBER,
  EV_COMMNT VARCHAR2(2000)  
);


--장바구니
CREATE TABLE BASKET(
  BAS_NO NUMBER(11),
  USER_NO NUMBER(11) REFERENCES USERS(USER_NO),
  REC_LIST NUMBER(11),
  REC_NO NUMBER(11) REFERENCES RECIEPE(REC_NO)  
);


--식단표
CREATE TABLE CARTE(
  CAR_NO NUMBER(11) PRIMARY KEY,
  CAR_DATE DATE DEFAULT SYSDATE,
  CAR_MORNING VARCHAR2(2000),
  CAR_LUNCH VARCHAR2(2000),
  CAR_DINNER VARCHAR2(2000)
);


--게시판
CREATE TABLE NOTICE(
  NOTICE_NO NUMBER(11) PRIMARY KEY,
  USER_NO NUMBER(11) REFERENCES USERS (USER_NO),
  NOTICE_TITLE VARCHAR2(100),
  NOTICE_CONTENTS VARCHAR2(2000),
  NOTICE_COUNT NUMBER(11),
  NOTICE_DATE DATE DEFAULT SYSDATE,
  FILE_NO NUMBER(11)  
);


--댓글
CREATE TABLE COMMENTS(
  COM_NO NUMBER(11) PRIMARY KEY,
  REC_NO NUMBER(11),
  USER_NO NUMBER(11) REFERENCES USERS (USER_NO),
  COM_CONTENTS VARCHAR2(2000),
  NICKNAME VARCHAR2(30),
  COM_PARRENT NUMBER(2)  
);


--FAQ
CREATE TABLE FAQ(
  FAQ_NO NUMBER(11) PRIMARY KEY,
  USER_NO NUMBER(11) REFERENCES USERS (USER_NO),
  FAQ_TITLE VARCHAR2(100),
  FAQ_CONTENTS VARCHAR2(2000)  
);


--영양성분표
CREATE TABLE NUTRITION_INFORMATION(
  NU_NO NUMBER PRIMARY KEY,
  NU_NAME VARCHAR2(100),
  PC_ENERGY VARCHAR2(20),
  PC_WATER VARCHAR2(20),
  PC_PROTEIN VARCHAR2(20),
  PC_FAT VARCHAR2(20),
  PC_ASH VARCHAR2(20),
  PC_CHO VARCHAR2(20),
  DF_TDF VARCHAR2(20),
  DF_SDF VARCHAR2(20),
  DF_IDF VARCHAR2(20),  
  MI_CALCIUM VARCHAR2(20),
  MI_PHOS_PHORUS VARCHAR2(20),
  MI_IRON VARCHAR2(20),
  MI_POTASSIUM VARCHAR2(20),
  MI_SODIUM VARCHAR2(20),
  VI_RETINOL_EV VARCHAR2(20),
  VI_RETINOL VARCHAR2(20),
  VI_B_CAROTENE VARCHAR2(20),
  VI_THIAMIN VARCHAR2(20),
  VI_RIBOFLAVIN VARCHAR2(20),
  VI_NIACIN VARCHAR2(20),
  VI_ASCORBIC_ACID VARCHAR2(20),
  SODIUM_CE VARCHAR2(20),
  REFUSE VARCHAR2(20),
  NU_SOURCE VARCHAR2(100)
);

COMMIT;