package finalfantasy.hanbab.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import finalfantasy.hanbab.vo.UserVO;


@Repository
public class SampleDAO {
	
	private static final String NAMESPACE = "userMapper.";

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	public UserVO loginUser(UserVO vo) {
		return sqlSession.selectOne(NAMESPACE + "loginCheck", vo);
	}
	

}
