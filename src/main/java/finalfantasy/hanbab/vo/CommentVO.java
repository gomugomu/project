package finalfantasy.hanbab.vo;

public class CommentVO {
	private int comNo;
	private int recNo;
	private int userNo;
	private String comContents;
	private String nickName;
	private int comParrent;
	
	public CommentVO(){}

	public int getComNo() {
		return comNo;
	}

	public void setComNo(int comNo) {
		this.comNo = comNo;
	}

	public int getRecNo() {
		return recNo;
	}

	public void setRecNo(int recNo) {
		this.recNo = recNo;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getComContents() {
		return comContents;
	}

	public void setComContents(String comContents) {
		this.comContents = comContents;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getComParrent() {
		return comParrent;
	}

	public void setComParrent(int comParrent) {
		this.comParrent = comParrent;
	}
	
	
}
