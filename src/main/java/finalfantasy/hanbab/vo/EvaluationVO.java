package finalfantasy.hanbab.vo;

public class EvaluationVO {
	public int evNo;
	public int conNo;
	public int recNo;
	public int userNo;
	public int evScore;
	public String evComment;
	
	public EvaluationVO(){}

	public int getEvNo() {
		return evNo;
	}

	public void setEvNo(int evNo) {
		this.evNo = evNo;
	}

	public int getConNo() {
		return conNo;
	}

	public void setConNo(int conNo) {
		this.conNo = conNo;
	}

	public int getRecNo() {
		return recNo;
	}

	public void setRecNo(int recNo) {
		this.recNo = recNo;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public int getEvScore() {
		return evScore;
	}

	public void setEvScore(int evScore) {
		this.evScore = evScore;
	}

	public String getEvComment() {
		return evComment;
	}

	public void setEvComment(String evComment) {
		this.evComment = evComment;
	}
	
	
	
}
