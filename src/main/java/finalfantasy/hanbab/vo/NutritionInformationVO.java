package finalfantasy.hanbab.vo;

public class NutritionInformationVO {
	private int nuNo;
	private String nuName;
	private String pcEnergy;
	private String pcWater;
	private String pcProtein;
	private String pcFat;
	private String pcAsh;
	private String pcCho;
	private String dfTdf;
	private String dfSdf;
	private String dfIdf;
	private String miCalcium;
	private String miPhosPhorus;
	private String miIron;
	private String miPotassium;
	private String miSodium;
	private String viRetinolEv;
	private String viRetinol;
	private String viBCarotene;
	private String viThiamin;
	private String viRiboflavin;
	private String viNiacin;
	private String viascorbic;
	private String sodiumCe;
	private String refuse;
	private String nuSource;
	
	public NutritionInformationVO(){}

	public int getNuNo() {
		return nuNo;
	}

	public void setNuNo(int nuNo) {
		this.nuNo = nuNo;
	}

	public String getNuName() {
		return nuName;
	}

	public void setNuName(String nuName) {
		this.nuName = nuName;
	}

	public String getPcEnergy() {
		return pcEnergy;
	}

	public void setPcEnergy(String pcEnergy) {
		this.pcEnergy = pcEnergy;
	}

	public String getPcWater() {
		return pcWater;
	}

	public void setPcWater(String pcWater) {
		this.pcWater = pcWater;
	}

	public String getPcProtein() {
		return pcProtein;
	}

	public void setPcProtein(String pcProtein) {
		this.pcProtein = pcProtein;
	}

	public String getPcFat() {
		return pcFat;
	}

	public void setPcFat(String pcFat) {
		this.pcFat = pcFat;
	}

	public String getPcAsh() {
		return pcAsh;
	}

	public void setPcAsh(String pcAsh) {
		this.pcAsh = pcAsh;
	}

	public String getPcCho() {
		return pcCho;
	}

	public void setPcCho(String pcCho) {
		this.pcCho = pcCho;
	}

	public String getDfTdf() {
		return dfTdf;
	}

	public void setDfTdf(String dfTdf) {
		this.dfTdf = dfTdf;
	}

	public String getDfSdf() {
		return dfSdf;
	}

	public void setDfSdf(String dfSdf) {
		this.dfSdf = dfSdf;
	}

	public String getDfIdf() {
		return dfIdf;
	}

	public void setDfIdf(String dfIdf) {
		this.dfIdf = dfIdf;
	}

	public String getMiCalcium() {
		return miCalcium;
	}

	public void setMiCalcium(String miCalcium) {
		this.miCalcium = miCalcium;
	}

	public String getMiPhosPhorus() {
		return miPhosPhorus;
	}

	public void setMiPhosPhorus(String miPhosPhorus) {
		this.miPhosPhorus = miPhosPhorus;
	}

	public String getMiIron() {
		return miIron;
	}

	public void setMiIron(String miIron) {
		this.miIron = miIron;
	}

	public String getMiPotassium() {
		return miPotassium;
	}

	public void setMiPotassium(String miPotassium) {
		this.miPotassium = miPotassium;
	}

	public String getMiSodium() {
		return miSodium;
	}

	public void setMiSodium(String miSodium) {
		this.miSodium = miSodium;
	}

	public String getViRetinolEv() {
		return viRetinolEv;
	}

	public void setViRetinolEv(String viRetinolEv) {
		this.viRetinolEv = viRetinolEv;
	}

	public String getViRetinol() {
		return viRetinol;
	}

	public void setViRetinol(String viRetinol) {
		this.viRetinol = viRetinol;
	}

	public String getViBCarotene() {
		return viBCarotene;
	}

	public void setViBCarotene(String viBCarotene) {
		this.viBCarotene = viBCarotene;
	}

	public String getViThiamin() {
		return viThiamin;
	}

	public void setViThiamin(String viThiamin) {
		this.viThiamin = viThiamin;
	}

	public String getViRiboflavin() {
		return viRiboflavin;
	}

	public void setViRiboflavin(String viRiboflavin) {
		this.viRiboflavin = viRiboflavin;
	}

	public String getViNiacin() {
		return viNiacin;
	}

	public void setViNiacin(String viNiacin) {
		this.viNiacin = viNiacin;
	}

	public String getViascorbic() {
		return viascorbic;
	}

	public void setViascorbic(String viascorbic) {
		this.viascorbic = viascorbic;
	}

	public String getSodiumCe() {
		return sodiumCe;
	}

	public void setSodiumCe(String sodiumCe) {
		this.sodiumCe = sodiumCe;
	}

	public String getRefuse() {
		return refuse;
	}

	public void setRefuse(String refuse) {
		this.refuse = refuse;
	}

	public String getNuSource() {
		return nuSource;
	}

	public void setNuSource(String nuSource) {
		this.nuSource = nuSource;
	}
	
	
}
