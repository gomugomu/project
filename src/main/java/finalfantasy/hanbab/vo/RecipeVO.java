package finalfantasy.hanbab.vo;

public class RecipeVO {
	private int recNo;
	private int recDep;
	private String recTitle;
	private String recContents;
	private String recFood;
	private String recIngredient;
	private int userNo;
	private int recScore;
	private int recCount;
	private int fileNo;
	
	public RecipeVO(){}

	public int getRecNo() {
		return recNo;
	}

	public void setRecNo(int recNo) {
		this.recNo = recNo;
	}

	public int getRecDep() {
		return recDep;
	}

	public void setRecDep(int recDep) {
		this.recDep = recDep;
	}

	public String getRecTitle() {
		return recTitle;
	}

	public void setRecTitle(String recTitle) {
		this.recTitle = recTitle;
	}

	public String getRecContents() {
		return recContents;
	}

	public void setRecContents(String recContents) {
		this.recContents = recContents;
	}

	public String getRecFood() {
		return recFood;
	}

	public void setRecFood(String recFood) {
		this.recFood = recFood;
	}

	public String getRecIngredient() {
		return recIngredient;
	}

	public void setRecIngredient(String recIngredient) {
		this.recIngredient = recIngredient;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public int getRecScore() {
		return recScore;
	}

	public void setRecScore(int recScore) {
		this.recScore = recScore;
	}

	public int getRecCount() {
		return recCount;
	}

	public void setRecCount(int recCount) {
		this.recCount = recCount;
	}

	public int getFileNo() {
		return fileNo;
	}

	public void setFileNo(int fileNo) {
		this.fileNo = fileNo;
	}
	
	
}
