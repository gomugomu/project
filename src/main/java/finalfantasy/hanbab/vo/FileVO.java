package finalfantasy.hanbab.vo;

public class FileVO {
	private int fileNo;
	private int fileSize;
	private String fileOriName;
	private String fileSaveRoot;
	private String fileRename;
	
	public FileVO(){}

	public int getFileNo() {
		return fileNo;
	}

	public void setFileNo(int fileNo) {
		this.fileNo = fileNo;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileOriName() {
		return fileOriName;
	}

	public void setFileOriName(String fileOriName) {
		this.fileOriName = fileOriName;
	}

	public String getFileSaveRoot() {
		return fileSaveRoot;
	}

	public void setFileSaveRoot(String fileSaveRoot) {
		this.fileSaveRoot = fileSaveRoot;
	}

	public String getFileRename() {
		return fileRename;
	}

	public void setFileRename(String fileRename) {
		this.fileRename = fileRename;
	}
	
	
}
