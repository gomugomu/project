package finalfantasy.hanbab.vo;

public class ContestVO {
	private int conNo;
	private String sponsorId;
	private String conEntry;
	private String recEntry;
	private int fileNo;
	private String conTitle;
	private String conContents;
	private int conCount;
	
	public ContestVO(){}

	public int getConNo() {
		return conNo;
	}

	public void setConNo(int conNo) {
		this.conNo = conNo;
	}

	public String getSponsorId() {
		return sponsorId;
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getConEntry() {
		return conEntry;
	}

	public void setConEntry(String conEntry) {
		this.conEntry = conEntry;
	}

	public String getRecEntry() {
		return recEntry;
	}

	public void setRecEntry(String recEntry) {
		this.recEntry = recEntry;
	}

	public int getFileNo() {
		return fileNo;
	}

	public void setFileNo(int fileNo) {
		this.fileNo = fileNo;
	}

	public String getConTitle() {
		return conTitle;
	}

	public void setConTitle(String conTitle) {
		this.conTitle = conTitle;
	}

	public String getConContents() {
		return conContents;
	}

	public void setConContents(String conContents) {
		this.conContents = conContents;
	}

	public int getConCount() {
		return conCount;
	}

	public void setConCount(int conCount) {
		this.conCount = conCount;
	}
	
	
}
