package finalfantasy.hanbab.vo;

import java.sql.Date;

public class CarteVO {
	private int carNo;
	private Date carDate;
	private String carMorning;
	private String carLunch;
	private String carDinner;
	
	public CarteVO(){}

	public int getCarNo() {
		return carNo;
	}

	public void setCarNo(int carNo) {
		this.carNo = carNo;
	}

	public Date getCarDate() {
		return carDate;
	}

	public void setCarDate(Date carDate) {
		this.carDate = carDate;
	}

	public String getCarMorning() {
		return carMorning;
	}

	public void setCarMorning(String carMorning) {
		this.carMorning = carMorning;
	}

	public String getCarLunch() {
		return carLunch;
	}

	public void setCarLunch(String carLunch) {
		this.carLunch = carLunch;
	}

	public String getCarDinner() {
		return carDinner;
	}

	public void setCarDinner(String carDinner) {
		this.carDinner = carDinner;
	}
	
	
	
}
