package finalfantasy.hanbab.vo;

public class BasketVO {
	private int basNo;
	private int userId;
	private int recList;
	private int recNo;
	
	public BasketVO(){}

	public int getBasNo() {
		return basNo;
	}

	public void setBasNo(int basNo) {
		this.basNo = basNo;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRecList() {
		return recList;
	}

	public void setRecList(int recList) {
		this.recList = recList;
	}

	public int getRecNo() {
		return recNo;
	}

	public void setRecNo(int recNo) {
		this.recNo = recNo;
	}
	
	
}
