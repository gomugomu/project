package finalfantasy.hanbab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import finalfantasy.hanbab.dao.SampleDAO;
import finalfantasy.hanbab.vo.UserVO;


@Service
public class SampleService {
	
	@Autowired
	private SampleDAO LoginDAO;

	public UserVO loginUser(UserVO vo) throws Exception {
		return LoginDAO.loginUser(vo);
	}

}